// Don't Count the Waves
// Processing 3.2.1
// 2017.09.17
// 10fps x 6s

/* ---------------------------------------------------------------------- */
class Petals {

  int cntWidthMax;
  int divRotate;
  int cntRotateMax;
  float basePetalSize;
  float baseColor;
  float petalBaseFrom, petalBaseTo, petalDivFrom, petalDivTo;

  Petals() {
    divRotate = 8;	// divide 360 degree
    cntRotateMax = 30 * 360 * divRotate;		// draw while rotating
    cntWidthMax = 8; //10;	// repeat same shape with different ellipse size
    basePetalSize = 300.0; //360.0;
    //    baseColor = map(random(1.0), 0.0, 1.0, 210.0, 360.0);
    baseColor = random(360.0);
  }

  void drawPetals() {

    float hueDiv = 30;
    if (baseColor > 50 && baseColor < 160) {
      // yellow and green
      hueDiv = 60;
    }
    
    for (int cntWidth = 1; cntWidth <= cntWidthMax; ++cntWidth) {

      float noiseHue = noiseHueStart + cntWidth / 300;
      float noiseSat = noiseSatStart;
      float noiseBri = noiseBriStart;
      float noiseAlp = noiseAlpStart;
      float noiseShp = noiseShpStart;
      float sumRotation = 0;

      pushMatrix();
      
      for (int cntRotate = 0; cntRotate < cntRotateMax; ++cntRotate) {


        // rotate fixed degree and calculate the plot point
        float rotation = 1.0 / divRotate;
        canvasRotation(rotation);
        sumRotation += rotation;

        rotate(radians(rotation));


        float idxW = 0.0;
        //        float idxH = basePetalSize * sin(radians(sumRotation / (50.0 + 5 * cos(radians(2.0 * cntRotate))))) * map(noise(noiseShp), 0.0, 1.0, 0.8, 1.2);
        float idxH = basePetalSize * sin(radians((rotation * cntRotate) / (50.0 + 5.0 * cos(radians(2.0 * cntRotate))))) * map(noise(noiseShp), 0.0, 1.0, 0.8, 1.2);

        /*
          float brushHue = (baseColor + 360 + map(noise(noiseHue), 0.0, 1.0, -60.0, 60)) % 360;
          float brushSat = map(noise(noiseSat), 0.0, 1.0, 50.0, 100.0);
          float brushSiz = map(noise(noiseBri), 0.0, 1.0, 0.0, 1.0 * cntWidth);
          float brushBri = map(noise(noiseBri), 0.0, 1.0, 0.0, 100.0) / cntWidth;
          float brushAlp = map(noise(noiseAlp), 0.0, 1.0, 0.0, 100.0);
        */

        float brushHue = (baseColor + 360.0 + map(abs(idxH / basePetalSize), 0.0, 1.2, -30.0, 30.0)) % 360.0; // Analogy
        //        float brushHue = (baseColor + 360.0 + map(noise(noiseShp) * cntRotate / cntRotateMax, 0.0, 1.0, -hueDiv, hueDiv)) % 360.0; // Analogy
        //        float brushHue = (baseColor + 360.0 + map(noise(noiseShp) + abs(idxH / basePetalSize), 0.0, 2.0, -30.0, 30.0)) % 360.0; // Analogy
        //        float brushHue = (baseColor + 360.0 + map(cntRotate / cntRotateMax, 0.0, 1.0, 0.0, 90.0)) % 360.0;  // Intermediate
        //        float brushHue = (baseColor + 360.0 + map(cntRotate / cntRotateMax, 0.0, 1.0, 0.0, 90.0) + map(noise(noiseHue), 0.0, 1.0, -30.0, 30.0)) % 360.0;  // Intermediate + Analogy
        //        float brushHue = (baseColor + 360.0 + map(noise(noiseHue), 0.0, 1.0, -30.0, 30.0)) % 360.0;  // Intermediate + Analogy
        //        float brushSat = map(noise(noiseSat), 0.0, 1.0, 30.0, 100.0) * map(cntWidth / cntWidthMax, 0.0, 1.0, 1.0, 2.0);
        float brushSat = map(noise(noiseSat), 0.0, 1.0, 40.0, 100.0);
        float brushBri = map(noise(noiseBri), 0.0, 1.0, 10.0, 50.0) / cntWidth;
        float brushAlp = map(noise(noiseAlp), 0.0, 1.0, 60.0, 100.0);
        float brushSiz = map(noise(noiseBri), 0.0, 1.0, 0.0, 1.0 * cntWidth);
        drawLine(idxW, idxH, brushHue, brushSat, brushBri, brushAlp, brushSiz);

        // weird shape
        idxH *= noise(noiseAlp) * 1.6;	// I don't mind using any noise
        /*
          if (abs(idxH) > 1.0) {
          brushBri *= (idxH / abs(idxH));
          }
        */
        //        brushHue = (brushHue + map(noise(noiseBri), 0.0, 1.0, -60.0, 60)) % 360;
        brushHue = (baseColor + 360 + map(noise(noiseShp), 0.0, 1.0, -30.0, 30)) % 360; // Analogy
        //        brushHue = (brushHue + 90) % 360;  // Intermediate
        brushBri *= idxH / basePetalSize * 0.4;
        brushAlp *= 0.2;
        brushSiz *= idxH / basePetalSize * 10.0;
        drawLine(idxW, idxH, brushHue, brushSat, brushBri, brushAlp, brushSiz);

        noiseHue += 0.001;
        noiseSat += 0.003;
        noiseBri += 0.005;
        noiseAlp += 0.005;
        noiseShp += 0.002;

      }
      popMatrix();

      canvasRotation(-cntRotateMax);

    }

  }

  void drawLine(float idxW, float idxH, float brushHue, float brushSat, float brushBri, float brushAlp, float brushSiz) {
    //    pushMatrix();
    //    translate(idxW, idxH);
    fill(brushHue, brushSat, brushBri, brushAlp);
    ellipse(idxW, idxH, brushSiz, brushSiz);
    //    popMatrix();
  }
  
  void canvasRotation(float degrees) {
    //    rotate(radians(degrees));
  }

}

/* ---------------------------------------------------------------------- */
Petals pt;
float noiseShpStart = 0.0; //random(100.0); //random(0.5, 3.0);
float noiseHueStart = random(100.0);
float noiseBriStart = random(100.0);
float noiseAlpStart = random(100.0);
float noiseSatStart = random(100.0);

void setup() {

  size(720, 720);
  colorMode(HSB, 360, 100, 100, 100);
  blendMode(SCREEN);
  noiseSeed(0);
  smooth();
  noStroke();
  //  noLoop();
  //  frameRate(1);

  pt = new Petals();  

}

void draw() {

  int frameCntMax = 24 * 6;
  float frameRatio = map(frameCount, 0, frameCntMax, 0.0, 1.0);
  
  background(0, 0, 0);
  translate(width / 2, height / 2);
  rotate(-PI * 0.02 * frameRatio);

  pt.drawPetals();

  /*
    noiseHueStart += 0.010;
    noiseSatStart += 0.004;
    noiseBriStart -= 0.006;
    noiseAlpStart -= 0.012;
    noiseShpStart += 0.004;
  */
  
  noiseHueStart += 0.006;
  noiseSatStart += 0.003;
  noiseBriStart -= 0.004;
  noiseAlpStart -= 0.008;
  noiseShpStart = sin(PI * frameRatio) * 0.05;
  
  saveFrame("frames/####.png");
  if (frameCount >= frameCntMax) {
    exit();
  }
  
}
